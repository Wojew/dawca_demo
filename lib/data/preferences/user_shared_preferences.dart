import '../../domain/domain.dart';

class UserSharedPreferences {
  static final _instance = UserSharedPreferences._();

  factory UserSharedPreferences() => _instance;

  UserSharedPreferences._();

  //Random data
  User getUser() => User(
        gender: Constans.man,
        badgeType: Constans.honoraryBloodDonor,
        donations: [
          Donation(
            amount: 450,
            timestamp: 1611074796,
            type: Constans.plasma,
          ),
          Donation(
            amount: 450,
            timestamp: 1611074830,
            type: Constans.fullBlood,
            comment: "Comment",
          ),
        ],
        legitimationNumber: "ABC12345",
      );

  bool hasUser() => true;
}
