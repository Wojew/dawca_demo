import '../../../data/data.dart';
import '../../domain.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc()
      : super(
          AuthenticationUninitialized(),
        );

  @override
  Stream<AuthenticationState> mapEventToState(AuthenticationEvent event) async* {
    if (event is AuthenticationInitApp) {
      yield* _mapInitAppToState();
    } else if (event is AuthenticationLogIn) {
      yield AuthenticationAuthenticated();
    } else if (event is AuthenticationLogOut) {
      yield AuthenticationUnauthenticated();
    }
  }

  Stream<AuthenticationState> _mapInitAppToState() async* {
    final isUserLoggedIn = UserSharedPreferences().hasUser();
    if (isUserLoggedIn) {
      yield AuthenticationAuthenticated();
    } else {
      yield AuthenticationUnauthenticated();
    }
  }
}
