import '../../domain.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class AuthenticationInitApp extends AuthenticationEvent {}

class AuthenticationLogIn extends AuthenticationEvent {}

class AuthenticationLogOut extends AuthenticationEvent {}
