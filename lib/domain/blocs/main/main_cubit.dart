import '../../domain.dart';

class MainCubit extends Cubit<int> {
  MainCubit() : super(0);

  homeScreen() => emit(0);

  diaryScreen() => emit(1);

  addDonationScreen() => emit(2);

  donorScreen() => emit(3);

  profileScreen() => emit(4);
}
