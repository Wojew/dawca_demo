import '../../../domain.dart';

class HomeWidgetData extends Equatable {
  final String badgeTypeTextId;
  final double dropCurrentValue;
  final String fullBloodNextDonationDateAsText;
  final int fullBloodNextDonationDaysLeft;
  final int totalDonationsAmount;
  final String lastDonationTypeTextId;
  final String lastDonationDateAsText;
  final String legitimationNumber;

  const HomeWidgetData({
    @required this.badgeTypeTextId,
    @required this.dropCurrentValue,
    @required this.fullBloodNextDonationDateAsText,
    @required this.fullBloodNextDonationDaysLeft,
    @required this.totalDonationsAmount,
    @required this.lastDonationTypeTextId,
    @required this.lastDonationDateAsText,
    @required this.legitimationNumber,
  }) : assert(
          badgeTypeTextId != null &&
              dropCurrentValue != null &&
              fullBloodNextDonationDateAsText != null &&
              fullBloodNextDonationDaysLeft != null &&
              totalDonationsAmount != null &&
              lastDonationTypeTextId != null &&
              lastDonationDateAsText != null &&
              legitimationNumber != null,
        );

  @override
  List<Object> get props => [
        badgeTypeTextId,
        dropCurrentValue,
        fullBloodNextDonationDateAsText,
        fullBloodNextDonationDaysLeft,
        totalDonationsAmount,
        lastDonationTypeTextId,
        lastDonationDateAsText,
        legitimationNumber,
      ];
}
