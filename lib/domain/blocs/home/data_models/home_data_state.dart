import '../../../domain.dart';

class HomeDataState {
  final User _user;

  HomeDataState() : _user = UserSharedPreferences().getUser();

  //Random data
  HomeWidgetData toHomeWidgetData() => HomeWidgetData(
        dropCurrentValue: 0.5,
        badgeTypeTextId: _getBadgeTypeTextId(),
        fullBloodNextDonationDateAsText: "20 lut 2021",
        fullBloodNextDonationDaysLeft: 1,
        totalDonationsAmount: _getTotalDonationsAmount(),
        lastDonationDateAsText: _getLastDonateFormatedData(),
        lastDonationTypeTextId: _getLastDonationTypeTextId(),
        legitimationNumber: _user.legitimationNumber,
      );

  Donation _getLastDonation() => _user.donations.last;

  DateTime _getLastDonateDate() => DateTime.fromMicrosecondsSinceEpoch(
        _getLastDonation().timestamp,
      );

  String _getLastDonateFormatedData() => DateHelper().formatDate(
        _getLastDonateDate(),
      );

  String _getLastDonationTypeTextId() {
    switch (_getLastDonation().type) {
      case Constans.plasma:
        return "plasma";
      case Constans.fullBlood:
      default:
        return "full_blood";
    }
  }

  String _getBadgeTypeTextId() {
    switch (_user.badgeType) {
      case Constans.honoredHonoraryBloodDonor3rdDegree:
        return "honored_honorary_blood_donor_3rd_degree";
      case Constans.honoraryBloodDonor:
      default:
        return "honorary_blood_donor";
    }
  }

  int _getTotalDonationsAmount() => _user.donations
      .map(
        (donation) => donation.amount,
      )
      .fold(
        0,
        (currentValue, amount) => currentValue + amount,
      );
}
