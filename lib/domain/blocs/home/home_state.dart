import '../../domain.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeLoading extends HomeState {}

class HomeIdle extends HomeState {
  final HomeWidgetData widgetData;

  const HomeIdle({
    @required this.widgetData,
  }) : assert(widgetData != null);

  @override
  List<Object> get props => [widgetData];
}
