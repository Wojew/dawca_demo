import '../../../data/data.dart';
import '../../domain.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final HomeDataState _homeDataState;

  HomeBloc()
      : _homeDataState = HomeDataState(),
        super(
          HomeLoading(),
        );

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is HomeFetchData) {
      yield* _mapFetchDataToState();
    }
  }

  Stream<HomeState> _mapFetchDataToState() async* {
    yield HomeIdle(
      widgetData: _homeDataState.toHomeWidgetData(),
    );
  }
}
