import '../domain.dart';

class Donation {
  final int amount;
  final int timestamp;
  final int type;
  final String comment;

  Donation({
    @required this.amount,
    @required this.timestamp,
    @required this.type,
    this.comment,
  }) : assert(
          amount != null && timestamp != null && type != null,
        );
}
