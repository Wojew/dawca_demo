import 'package:meta/meta.dart';

class BottomNavigationItem {
  final String assetPath;

  const BottomNavigationItem({
    @required this.assetPath,
  });
}
