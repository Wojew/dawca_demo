import '../domain.dart';

class User {
  final int gender;
  final int badgeType;
  final List<Donation> donations;
  final String legitimationNumber;

  const User({
    @required this.gender,
    @required this.badgeType,
    @required this.donations,
    this.legitimationNumber,
  }) : assert(
          gender != null && badgeType != null && donations != null,
        );
}
