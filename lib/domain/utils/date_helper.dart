import 'package:intl/intl.dart';

class DateHelper {
  static final _instance = DateHelper._();

  factory DateHelper() => _instance;

  DateHelper._();

  String formatDate(DateTime date) => DateFormat('d MMM yyyy', 'pl_PL').format(date);
}
