class Constans {
  //Gender
  static const man = 1;
  static const woman = 2;

  //BADGE
  static const honoraryBloodDonor = 1;
  static const honoredHonoraryBloodDonor3rdDegree = 2;

  //BLOOD DONATE TYPES
  static const fullBlood = 1;
  static const plasma = 2;
}
