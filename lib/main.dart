import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'domain/domain.dart';
import 'ui/ui.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  runApp(
    BlocProvider<AuthenticationBloc>(
      create: (context) => AuthenticationBloc(),
      child: App(),
    ),
  );
}

class App extends StatefulWidget {
  final stackKey = GlobalKey<NavigatorState>();

  @override
  _App createState() => _App();
}

class _App extends State<App> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<AuthenticationBloc>(context).add(
      AuthenticationInitApp(),
    );
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
        navigatorKey: widget.stackKey,
        debugShowCheckedModeBanner: false,
        theme: AppStyles.appTheme,
        supportedLocales: I18n.supportedLocales,
        localizationsDelegates: [
          I18n.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          DefaultCupertinoLocalizations.delegate
        ],
        onGenerateTitle: (context) => I18n.of(context).translate("app_name"),
        home: FlavorBanner(
          child: _buildContent(),
        ),
      );

  Widget _buildContent() => BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is AuthenticationAuthenticated) {
            return MainStack();
          } else if (state is AuthenticationUnauthenticated) {
            return StartStack();
          } else if (state is AuthenticationUninitialized) {
            return SplashScreen();
          }
          return SizedBox.shrink();
        },
      );
}
