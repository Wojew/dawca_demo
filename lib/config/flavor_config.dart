import 'package:meta/meta.dart';

enum Flavor { development, staging, production }

class FlavorConfig {
  final Flavor flavor;
  final String bannerText;
  static FlavorConfig _instance;

  factory FlavorConfig({
    @required Flavor flavor,
  }) =>
      _instance ??= FlavorConfig._internal(flavor);

  FlavorConfig._internal(this.flavor) : bannerText = _getBannerName(flavor);

  static String _getBannerName(Flavor flavor) {
    switch (flavor) {
      case Flavor.staging:
        return "STAG";
      case Flavor.production:
        return "PROD";
      case Flavor.development:
      default:
        return "DEV";
    }
  }

  static FlavorConfig get instance => _instance;

  static bool isProduction() => _instance.flavor == Flavor.production;
  static bool isDevelopment() => _instance.flavor == Flavor.development;
  static bool isStaging() => _instance.flavor == Flavor.staging;
}
