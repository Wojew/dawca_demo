import 'config/config.dart';
import 'main.dart' as app;

main() {
  FlavorConfig(
    flavor: Flavor.production,
  );
  app.main();
}
