import '../../ui.dart';

class MainDestination {
  static const root = "/";
  static const addDonation = "/addDonation";
  static const donationDetails = "/donationDetails";
  static const settings = "/settings";
  static const placeDetails = "/placeDetails";
}

class MainStack extends StatefulWidget {
  final stackKey = GlobalKey<NavigatorState>();

  @override
  _MainStack createState() => _MainStack();
}

class _MainStack extends State<MainStack> {
  Future<bool> _onWillPop() async {
    final isRoot = !(await widget.stackKey.currentState.maybePop());
    return isRoot;
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: _onWillPop,
        child: Navigator(
          key: widget.stackKey,
          initialRoute: MainDestination.root,
          onGenerateRoute: (settings) => MaterialPageRoute(
            settings: settings,
            builder: (context) {
              final name = settings.name;
              // final arguments = settings.arguments;
              switch (name) {
                case MainDestination.root:
                  return MainScreenProvider();
                // case MainDestination.addDonation:
                //   return AddDonationScreenProvider(args: arguments);
                // case MainDestination.donationDetails:
                //   return DonationDetailsScreenProvider(args: arguments);
                // case MainDestination.settings:
                //   return SettingsScreenProvider(args: arguments);
                // case MainDestination.placeDetails:
                //   return PlaceDetailsScreenProvider(args: arguments);
                default:
                  return SizedBox.shrink();
              }
            },
          ),
        ),
      );
}
