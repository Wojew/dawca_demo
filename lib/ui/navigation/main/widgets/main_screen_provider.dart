import '../../../ui.dart';

class MainScreenProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (_) => MainCubit(),
        child: MainScreen(),
      );
}
