import '../../../ui.dart';

class HomeScreenProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (_) => HomeBloc(),
        child: HomeScreen(),
      );
}
