import '../../ui.dart';

class StartDestination {
  static const root = "/";
}

class StartStack extends StatefulWidget {
  final stackKey = GlobalKey<NavigatorState>();

  @override
  _StartStack createState() => _StartStack();
}

class _StartStack extends State<StartStack> {
  Future<bool> _onWillPop() async {
    final isRoot = !(await widget.stackKey.currentState.maybePop());
    return isRoot;
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: _onWillPop,
        child: Navigator(
          key: widget.stackKey,
          initialRoute: StartDestination.root,
          onGenerateRoute: (settings) => MaterialPageRoute(
            settings: settings,
            builder: (context) {
              final name = settings.name;
              // final arguments = settings.arguments;
              switch (name) {
                case StartDestination.root:
                // return StartScreenProvider(args: arguments);
                default:
                  return SizedBox.shrink();
              }
            },
          ),
        ),
      );
}
