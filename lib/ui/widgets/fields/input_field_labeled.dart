import '../../ui.dart';

class InputFieldLabeled extends StatelessWidget {
  final String labelTextId;
  final TextEditingController controller;
  final bool enabled;
  final Function onPress;
  final List<TextInputFormatter> inputFormatters;
  final InputFormTextFieldType keyboardType;
  final bool isValid;
  final TextCapitalization textCapitalization;

  const InputFieldLabeled({
    @required this.labelTextId,
    @required this.controller,
    this.enabled = true,
    this.onPress,
    this.inputFormatters,
    this.keyboardType = InputFormTextFieldType.text,
    this.isValid = true,
    this.textCapitalization = TextCapitalization.none,
  })  : assert(labelTextId != null),
        assert(controller != null);

  @override
  Widget build(BuildContext context) => Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildLabel(context),
          _buildVerticalSpace(),
          InputField(
            controller: controller,
            inputFormatters: inputFormatters,
            keyboardType: keyboardType,
            enabled: enabled,
            onPress: onPress,
            isValid: isValid,
            textCapitalization: textCapitalization,
          ),
        ],
      );

  _buildVerticalSpace() => SizedBox(
        height: Dimens.spaceTiny,
      );

  _buildLabel(BuildContext context) => Text(
        I18n.of(context).translate(labelTextId),
        style: TextStyles.labelBoldText,
      );
}
