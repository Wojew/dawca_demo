import '../../ui.dart';

enum InputFormTextFieldType {
  text,
  email,
  number,
  password,
  phone,
  numberWithDot,
}

class InputField extends StatelessWidget {
  final TextEditingController controller;
  final bool enabled;
  final Function onPress;
  final List<TextInputFormatter> inputFormatters;
  final InputFormTextFieldType keyboardType;
  final bool isValid;
  final TextCapitalization textCapitalization;

  const InputField({
    @required this.controller,
    this.enabled = true,
    this.onPress,
    this.inputFormatters,
    this.keyboardType = InputFormTextFieldType.text,
    this.isValid = true,
    this.textCapitalization = TextCapitalization.none,
  }) : assert(controller != null);

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: onPress,
        child: Container(
          decoration: _buildBoxDecoration(),
          child: TextFormField(
            controller: controller,
            style: TextStyles.fieldText,
            autocorrect: false,
            cursorColor: AppColors.greyPrimary,
            keyboardType: _getKeyboardType(),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(Dimens.spaceSmall),
              border: InputBorder.none,
              isDense: true,
            ),
            enabled: enabled,
            inputFormatters: inputFormatters,
            textCapitalization: textCapitalization,
          ),
        ),
      );

  _buildBoxDecoration() => isValid ? AppStyles.appBoxDecoration : AppStyles.appBoxDecorationInvalid;

  TextInputType _getKeyboardType() {
    switch (keyboardType) {
      case InputFormTextFieldType.email:
        return TextInputType.emailAddress;
      case InputFormTextFieldType.number:
        return TextInputType.number;
      case InputFormTextFieldType.numberWithDot:
        return TextInputType.numberWithOptions(decimal: true);
      case InputFormTextFieldType.phone:
        return TextInputType.phone;
      case InputFormTextFieldType.text:
      case InputFormTextFieldType.password:
      default:
        return TextInputType.text;
    }
  }
}
