import '../../config/config.dart';
import '../ui.dart';

class FlavorBanner extends StatelessWidget {
  final Widget child;

  const FlavorBanner({
    @required this.child,
  }) : assert(child != null);

  @override
  Widget build(BuildContext context) => FlavorConfig.isProduction()
      ? child
      : Stack(
          children: <Widget>[
            child,
            _buildBanner(context),
          ],
        );

  String _getBannerText() => FlavorConfig.instance.bannerText;

  Widget _buildBanner(BuildContext context) => SizedBox(
        width: 50,
        height: 50,
        child: CustomPaint(
          painter: BannerPainter(
            message: _getBannerText(),
            textDirection: Directionality.of(context),
            layoutDirection: Directionality.of(context),
            location: BannerLocation.topStart,
            color: Colors.green,
          ),
        ),
      );
}
