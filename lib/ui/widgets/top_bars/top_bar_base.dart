import '../../ui.dart';

enum TopBarLeftIcon {
  none,
  arrow,
  importExport,
}

enum TopBarRightIcon {
  none,
  options,
  logout,
  trash,
}

class TopBarBase extends StatelessWidget {
  final TopBarLeftIcon leftIcon;
  final TopBarRightIcon rightIcon;
  final Widget child;

  final Function() onPressLeftIcon;
  final Function() onPressChild;
  final Function() onPressRightIcon;

  final iconSize = Dimens.topBarIconSize;
  final barHeight = Dimens.topBarHeight;

  const TopBarBase({
    this.leftIcon = TopBarLeftIcon.none,
    this.rightIcon = TopBarRightIcon.none,
    this.child,
    this.onPressLeftIcon,
    this.onPressChild,
    this.onPressRightIcon,
  });

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Container(
            height: MediaQuery.of(context).padding.top,
            color: AppColors.whitePrimary,
          ),
          Container(
            height: barHeight,
            decoration: BoxDecoration(
              color: AppColors.whitePrimary,
              border: Border(
                bottom: AppStyles.appBorderSide.copyWith(
                  color: AppColors.greyPrimary.withOpacity(0.2),
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _getLeftIcon(),
                Flexible(
                  child: _getCenterWidget(),
                ),
                _getRightIcon()
              ],
            ),
          ),
        ],
      );

  _getLeftIcon() {
    final onPress = onPressLeftIcon ?? () {};
    final color = AppColors.greyPrimary;
    switch (leftIcon) {
      case TopBarLeftIcon.arrow:
        return ButtonWithIconRounded(
          buttonSize: barHeight,
          assetPath: "assets/icons/back_icon.svg",
          iconSize: iconSize,
          onPress: onPress,
          color: color,
        );
      case TopBarLeftIcon.importExport:
        return ButtonWithIconRounded(
          buttonSize: barHeight,
          assetPath: "assets/icons/import_export_icon.svg",
          iconSize: iconSize,
          onPress: onPress,
          color: color,
        );
      case TopBarLeftIcon.none:
      default:
        return SizedBox(
          width: barHeight,
          height: barHeight,
        );
    }
  }

  _getRightIcon() {
    final onPress = onPressRightIcon ?? () {};
    final color = AppColors.greyPrimary;
    switch (rightIcon) {
      case TopBarRightIcon.options:
        return ButtonWithIconRounded(
          buttonSize: barHeight,
          assetPath: "assets/icons/settings_icon.svg",
          iconSize: iconSize,
          onPress: onPress,
          color: color,
        );
      case TopBarRightIcon.logout:
        return ButtonWithIconRounded(
          buttonSize: barHeight,
          assetPath: "assets/icons/settings_icon.svg",
          iconSize: iconSize,
          onPress: onPress,
          color: color,
        );
      case TopBarRightIcon.trash:
        return ButtonWithIconRounded(
          buttonSize: barHeight,
          assetPath: "assets/icons/trash_icon.svg",
          iconSize: iconSize,
          onPress: onPress,
          color: color,
        );
      case TopBarRightIcon.none:
      default:
        return SizedBox(
          width: barHeight,
          height: barHeight,
        );
    }
  }

  _getCenterWidget() {
    if (child == null) {
      return Container();
    }
    final onPress = onPressChild ?? () {};
    return GestureDetector(
      onTap: onPress,
      child: child,
    );
  }
}
