import '../../ui.dart';

class TopBarScreen extends StatelessWidget {
  final TopBarLeftIcon leftIcon;
  final TopBarRightIcon rightIcon;
  final String textId;

  final Function() onPressLeftIcon;
  final Function() onPressText;
  final Function() onPressRightIcon;

  const TopBarScreen({
    this.leftIcon = TopBarLeftIcon.none,
    this.rightIcon = TopBarRightIcon.none,
    this.textId,
    this.onPressLeftIcon,
    this.onPressText,
    this.onPressRightIcon,
  });

  Widget _getText(BuildContext context) {
    if (textId != null) {
      return Text(
        I18n.of(context).translate(textId),
        textAlign: TextAlign.center,
        maxLines: 1,
        softWrap: false,
        overflow: TextOverflow.fade,
        style: TextStyles.h1Text,
      );
    }
    return null;
  }

  @override
  Widget build(BuildContext context) => TopBarBase(
        leftIcon: leftIcon,
        rightIcon: rightIcon,
        onPressLeftIcon: onPressLeftIcon,
        onPressChild: onPressText,
        onPressRightIcon: onPressRightIcon,
        child: _getText(context),
      );
}
