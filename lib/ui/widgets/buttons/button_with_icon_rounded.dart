import '../../ui.dart';

class ButtonWithIconRounded extends StatelessWidget {
  final double buttonSize;
  final String assetPath;
  final double iconSize;
  final Function onPress;
  final Color color;

  const ButtonWithIconRounded({
    @required this.buttonSize,
    @required this.assetPath,
    @required this.iconSize,
    @required this.onPress,
    this.color = AppColors.greyPrimary,
  })  : assert(buttonSize != null),
        assert(assetPath != null),
        assert(iconSize != null),
        assert(onPress != null);

  @override
  Widget build(BuildContext context) => SizedBox(
        width: buttonSize,
        height: buttonSize,
        child: RawMaterialButton(
          onPressed: onPress,
          elevation: 0,
          highlightElevation: 0,
          fillColor: Colors.transparent,
          shape: CircleBorder(),
          child: SvgPicture.asset(
            assetPath,
            fit: BoxFit.scaleDown,
            color: color,
            width: iconSize,
            height: iconSize,
          ),
        ),
      );
}
