import '../../ui.dart';

class TextWithData extends StatelessWidget {
  final String titleTextId;
  final String boldText;
  final String normalText;

  const TextWithData({
    @required this.titleTextId,
    @required this.boldText,
    this.normalText,
  })  : assert(titleTextId != null),
        assert(boldText != null);

  @override
  Widget build(BuildContext context) => Row(
        children: [
          Text(
            I18n.of(context).translate(titleTextId),
            style: TextStyles.normalText,
          ),
          Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              _buildBoldText(),
              _buildSpaceBetweenBoldAndNormalText(),
              _buildNormalText(),
            ],
          ),
        ],
      );

  _buildBoldText() => Text(
        boldText,
        style: TextStyles.normalBoldText,
        maxLines: 1,
        softWrap: false,
        overflow: TextOverflow.fade,
      );

  _buildSpaceBetweenBoldAndNormalText() => normalText != null
      ? SizedBox(
          width: Dimens.spaceTiny,
        )
      : SizedBox.shrink();

  _buildNormalText() => normalText != null
      ? Text(
          normalText,
          style: TextStyles.normalText,
        )
      : SizedBox.shrink();
}
