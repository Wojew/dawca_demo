import '../../ui.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => SafeArea(
        top: false,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _buildTopBar(),
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(
                  horizontal: Dimens.spaceMedium,
                ),
                child: Column(
                  children: [
                    _buildVerticalSpace(),
                    HomeDrop(),
                    _buildVerticalSpace(),
                    HomeMainInfoSection(),
                    _buildVerticalSpace(),
                    HomeNextDonationsInfoSection(),
                    _buildVerticalSpace(
                      space: Dimens.spaceHuge,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );

  _buildTopBar() => TopBarScreen(
        textId: "app_name",
      );

  _buildVerticalSpace({
    double space = Dimens.spaceBig,
  }) =>
      SizedBox(
        height: space,
      );
}
