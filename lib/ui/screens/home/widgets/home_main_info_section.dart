import '../../../ui.dart';

class HomeMainInfoSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildTitle(),
          _buildVerticalSpace(),
          _buildTotalDonationsAmount(),
          _buildLastDonationType(),
          _buildLastDonationDate(),
          _buildLegitimationNumber(),
        ],
      );

  _buildVerticalSpace() => SizedBox(
        height: Dimens.spaceTiny,
      );

  _buildContent(Widget Function(BuildContext, HomeIdle) buildChild) =>
      BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) => state is HomeIdle ? buildChild(context, state) : _buildEmpty(),
      );

  _buildTitle() => _buildContent(
        (_, state) => HomeSectionTitle(
          titleTextId: state.widgetData.badgeTypeTextId,
        ),
      );

  _buildTotalDonationsAmount() => _buildContent(
        (context, state) => TextWithData(
          titleTextId: "donated_total",
          boldText: "${state.widgetData.totalDonationsAmount}",
          normalText: I18n.of(context).translate("ml"),
        ),
      );

  _buildLastDonationType() => _buildContent(
        (context, state) => TextWithData(
          titleTextId: "last_entry_type",
          boldText: I18n.of(context).translate(state.widgetData.lastDonationTypeTextId),
        ),
      );

  _buildLastDonationDate() => _buildContent(
        (context, state) => TextWithData(
          titleTextId: "last_entry_date",
          boldText: state.widgetData.lastDonationDateAsText,
        ),
      );

  _buildLegitimationNumber() => _buildContent(
        (context, state) => TextWithData(
          titleTextId: "id_number",
          boldText: state.widgetData.legitimationNumber,
        ),
      );

  _buildEmpty() => SizedBox.shrink();
}
