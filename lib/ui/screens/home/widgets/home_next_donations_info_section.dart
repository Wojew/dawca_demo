import '../../../ui.dart';

class HomeNextDonationsInfoSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildTitle(),
          _buildVerticalSpace(),
          _buildNextFullBloodDonate(),
        ],
      );

  _buildVerticalSpace() => SizedBox(
        height: Dimens.spaceTiny,
      );

  _buildTitle() => HomeSectionTitle(
        titleTextId: "next_donations",
      );

  _buildNextFullBloodDonate() => BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) => state is HomeIdle
            ? TextWithData(
                titleTextId: "full_blood",
                boldText: state.widgetData.fullBloodNextDonationDateAsText,
                normalText:
                    "(${state.widgetData.fullBloodNextDonationDaysLeft} ${I18n.of(context).translate("days")})",
              )
            : _buildEmpty(),
      );

  _buildEmpty() => SizedBox.shrink();
}
