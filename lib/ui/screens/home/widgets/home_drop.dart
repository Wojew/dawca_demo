import '../../../ui.dart';

class HomeDrop extends StatelessWidget {
  @override
  Widget build(BuildContext context) => BlocBuilder<HomeBloc, HomeState>(
        builder: (_, state) =>
            state is HomeIdle ? _buildDrop(state.widgetData.dropCurrentValue) : _buildEmptyDrop(),
      );

  _buildDrop(double currentValue) => Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(width: 0.13 * Dimens.dropWidth),
          LiquidCustomProgressIndicator(
            value: currentValue,
            valueColor: AlwaysStoppedAnimation(
              AppColors.redPrimary,
            ),
            backgroundColor: AppColors.greyPrimary,
            direction: Axis.vertical,
            shapePath: _buildShapePath(),
          ),
        ],
      );

  _buildEmptyDrop() => _buildDrop(0);

  _buildShapePath() {
    final size = Size(
      Dimens.dropWidth,
      Dimens.dropHeight,
    );
    final Path path = Path()
      ..moveTo(size.width / 2, 0)
      ..cubicTo(
        0,
        size.height / 2,
        0,
        size.height,
        size.width / 2,
        size.height,
      )
      ..cubicTo(
        size.width + (0.13 * size.width),
        size.height - (0.04 * size.width),
        size.width * 5 / 6,
        size.height / 3,
        size.width / 2,
        0,
      );
    return path;
  }
}
