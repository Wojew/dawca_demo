import '../../../ui.dart';

class HomeSectionTitle extends StatelessWidget {
  final String titleTextId;

  const HomeSectionTitle({
    @required this.titleTextId,
  }) : assert(titleTextId != null);

  @override
  Widget build(BuildContext context) => Text(
        I18n.of(context).translate(titleTextId),
        style: TextStyles.h2Text,
      );
}
