import '../../../ui.dart';

class MainBottomNavigationItemBuilder {
  final BottomNavigationItem appBottomNavigationItem;

  const MainBottomNavigationItemBuilder({
    @required this.appBottomNavigationItem,
  }) : assert(appBottomNavigationItem != null);

  create() => BottomNavigationBarItem(
        activeIcon: _getImage(
          color: AppColors.redPrimary,
        ),
        icon: _getImage(),
        label: "",
      );

  Widget _getImage({
    Color color = AppColors.greyPrimary,
  }) =>
      appBottomNavigationItem.assetPath != null
          ? SvgPicture.asset(
              appBottomNavigationItem.assetPath,
              color: color,
            )
          : SizedBox.shrink();
}
