import '../../../ui.dart';

class MainBottomNavigationCenterItemBuilder extends StatelessWidget {
  final Function onPress;

  const MainBottomNavigationCenterItemBuilder({
    @required this.onPress,
  }) : assert(onPress != null);

  @override
  Widget build(BuildContext context) => FloatingActionButton(
        onPressed: onPress,
        backgroundColor: AppColors.whitePrimary,
        elevation: 2,
        highlightElevation: 2,
        focusElevation: 2,
        hoverElevation: 2,
        child: Image.asset(
          'assets/icons/nav_add_icon.png',
          fit: BoxFit.contain,
          width: Dimens.bottomNavigationBigIconSize,
          height: Dimens.bottomNavigationBigIconSize,
        ),
      );
}
