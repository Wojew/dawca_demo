import '../../../ui.dart';

class MainBottomNavigationBuilder extends StatelessWidget {
  final int currentIndex;
  final Function onPressItem;
  final _bottomNavigationItems = const [
    BottomNavigationItem(
      assetPath: 'assets/icons/nav_home_icon.svg',
    ),
    BottomNavigationItem(
      assetPath: 'assets/icons/nav_diary_icon.svg',
    ),
    BottomNavigationItem(
      assetPath: null,
    ),
    BottomNavigationItem(
      assetPath: 'assets/icons/nav_donor_icon.svg',
    ),
    BottomNavigationItem(
      assetPath: 'assets/icons/nav_profile_icon.svg',
    )
  ];

  const MainBottomNavigationBuilder({
    @required this.currentIndex,
    @required this.onPressItem,
  })  : assert(currentIndex != null),
        assert(onPressItem != null);

  @override
  Widget build(BuildContext context) => Container(
        height: _buildHeight(context),
        decoration: _buildBoxDecoration(),
        child: Theme(
          data: _buildThemeData(),
          child: _buildBottomNavigationBar(),
        ),
      );

  _buildBottomNavigationBar() => BottomNavigationBar(
        items: _getItems(),
        currentIndex: currentIndex,
        onTap: onPressItem,
        type: BottomNavigationBarType.fixed,
        iconSize: Dimens.bottomNavigationIconSize,
        selectedItemColor: AppColors.redPrimary,
        unselectedItemColor: AppColors.greyPrimary,
        backgroundColor: AppColors.whitePrimary,
        showSelectedLabels: false,
        showUnselectedLabels: false,
      );

  _getItems() => _bottomNavigationItems
      .map<BottomNavigationBarItem>(
        (item) => MainBottomNavigationItemBuilder(
          appBottomNavigationItem: item,
        ).create(),
      )
      .toList();

  _buildThemeData() => ThemeData(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
      );

  _buildHeight(BuildContext context) =>
      Dimens.bottomNavigationBarHeight + MediaQuery.of(context).padding.bottom;

  _buildBoxDecoration() => BoxDecoration(
        border: Border(
          top: AppStyles.appBorderSide.copyWith(
            color: AppColors.greyPrimary.withOpacity(0.2),
          ),
        ),
      );
}
