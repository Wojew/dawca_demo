import '../../ui.dart';

class MainScreen extends StatefulWidget {
  final Object args;

  const MainScreen({this.args});

  @override
  _MainScreen createState() => _MainScreen();
}

class _MainScreen extends State<MainScreen> {
  static const _homeScreenIndex = 0;
  static const _diaryScreenIndex = 1;
  static const _donorScreenIndex = 3;
  static const _profileScreenIndex = 4;

  @override
  Widget build(BuildContext context) => BlocBuilder<MainCubit, int>(
        builder: (context, currentIndex) => Scaffold(
          backgroundColor: AppColors.whitePrimary,
          body: _getScreen(currentIndex),
          floatingActionButton: Visibility(
            visible: MediaQuery.of(context).viewInsets.bottom == 0.0,
            child: MainBottomNavigationCenterItemBuilder(
              onPress: _onPressAdd,
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          bottomNavigationBar: MainBottomNavigationBuilder(
            currentIndex: currentIndex,
            onPressItem: _onPressBottomNavigationItem,
          ),
        ),
      );

  _onPressBottomNavigationItem(int index) {
    switch (index) {
      case _homeScreenIndex:
        context.read<MainCubit>().homeScreen();
        break;
      case _diaryScreenIndex:
        context.read<MainCubit>().diaryScreen();
        break;
      case _donorScreenIndex:
        context.read<MainCubit>().donorScreen();
        break;
      case _profileScreenIndex:
        context.read<MainCubit>().profileScreen();
        break;
    }
  }

  _onPressAdd() => Navigator.pushNamed(
        context,
        MainDestination.addDonation,
      );

  _getScreen(int currentIndex) {
    switch (currentIndex) {
      case _diaryScreenIndex:
      //   return DiaryScreenProvider();
      case _donorScreenIndex:
      //   return DonorScreenProvider();
      case _profileScreenIndex:
      //   return ProfileScreenProvider();
      case _homeScreenIndex:
      default:
        return HomeScreenProvider();
    }
  }
}
