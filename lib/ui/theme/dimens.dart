abstract class Dimens {
  //Global
  static const appBorderWidth = 0.5;
  static const appRadius = 8.0;

  //Texts
  static const logoFontSize = 30.0;
  static const h1FontSize = 24.0;
  static const h2FontSize = 18.0;
  static const fieldFontSize = 18.0;
  static const normalFontSize = 16.0;
  static const smallFontSize = 14.0;

  //Text fields
  static const textFieldHeight = 60.0;

  //Texts
  static const textIconSize = 20.0;

  //Buttons
  static const buttonHeight = 40.0;
  static const buttonImageSize = 50.0;

  //Navigation
  static const bottomNavigationIconSize = 24.0;
  static const bottomNavigationBigIconSize = 40.0;
  static const bottomNavigationBarHeight = 70.0;

  //Loading
  static const loadingSpinnerSize = 56.0;

  //Spacing
  static const spaceTiny = 4.0;
  static const spaceSmall = 8.0;
  static const spaceMedium = 16.0;
  static const spaceBig = 24.0;
  static const spaceHuge = 32.0;

  //Placeholders
  static const placeholderRadius = 30.0;
  static const iconPlaceholderSize = 40.0;

  //Top bar
  static const topBarHeight = 50.0;
  static const topBarIconSize = 22.0;

  //Drop
  static const dropWidth = 120.0;
  static const dropHeight = 130.0;

  static const dropSmallWidth = 90.0;
  static const dropSmallHeight = 100.0;

  //List item
  static const listBorderLeftAccent = 6.0;
  static const listRadius = 3.0;

  //Map
  static const mapHeight = 200.0;

  //Badge
  static const badgeSize = 100.0;

  //Switch
  static const switchHeight = 40.0;
}
