import '../ui.dart';

abstract class AppColors {
  static const whitePrimary = Color(0xFFFFFFFF);
  static const whiteSecondary = Color(0xFFF8F8F8);

  static const redPrimary = Color(0xFFD12C2C);
  static const redSecondary = Color(0xFFf05851);

  static const greyPrimary = Color(0xFF424343);
  static const greySecondary = Color(0xFFB0B8C9);

  static const blackPrimary = Color(0xFF000000);
  static const blackSecondary = Color(0xFF1E1C1E);

  static const yellowPrimary = Color(0xFFFED250);

  static const greenPrimary = Color(0xFF15BB9C);
}
