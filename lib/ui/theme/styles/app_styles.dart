import '../../ui.dart';

abstract class AppStyles {
  static final appTheme = ThemeData.light().copyWith(
    scaffoldBackgroundColor: AppColors.whitePrimary,
    splashColor: Colors.transparent,
  );

  static final appBorderSide = BorderSide(
    width: Dimens.appBorderWidth,
    color: AppColors.greyPrimary,
  );

  static final appBoxDecoration = BoxDecoration(
    color: AppColors.whitePrimary,
    borderRadius: BorderRadius.circular(Dimens.appRadius),
    border: Border.fromBorderSide(appBorderSide),
  );

  static final appBoxDecorationInvalid = AppStyles.appBoxDecoration.copyWith(
    border: Border.fromBorderSide(
      AppStyles.appBorderSide.copyWith(
        color: AppColors.redPrimary,
      ),
    ),
  );
}
