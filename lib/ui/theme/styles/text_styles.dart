import '../../ui.dart';

abstract class TextStyles {
  static const h1Text = TextStyle(
    fontFamily: AppFonts.futura,
    fontSize: Dimens.h1FontSize,
    color: AppColors.redPrimary,
    fontWeight: FontWeight.w700,
  );

  static const h2Text = TextStyle(
    fontFamily: AppFonts.futura,
    fontSize: Dimens.h2FontSize,
    color: AppColors.redPrimary,
  );

  static const labelBoldText = TextStyle(
    fontFamily: AppFonts.futura,
    fontSize: Dimens.fieldFontSize,
    color: AppColors.greyPrimary,
    fontWeight: FontWeight.w700,
  );

  static const fieldText = TextStyle(
    fontFamily: AppFonts.futura,
    fontSize: Dimens.fieldFontSize,
    color: AppColors.greyPrimary,
    fontStyle: FontStyle.italic,
  );

  static const normalText = TextStyle(
    fontFamily: AppFonts.futura,
    fontSize: Dimens.normalFontSize,
    color: AppColors.greyPrimary,
  );

  static const normalBoldText = TextStyle(
    fontFamily: AppFonts.futura,
    fontSize: Dimens.normalFontSize,
    color: AppColors.redPrimary,
    fontWeight: FontWeight.w700,
  );
}
