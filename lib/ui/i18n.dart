import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// Class for text translation
///
/// Example usage:
/// ```dart
/// I18n.of(context).translate("key")
/// ```
class I18n {
  final Locale locale;

  I18n(this.locale);

  Map<String, String> _localizedStrings;

  static I18n of(BuildContext context) => Localizations.of<I18n>(context, I18n);

  static const LocalizationsDelegate<I18n> delegate = _I18nDelegate();

  static const supportedLocales = [
    Locale('pl'),
  ];

  Future<void> load() async {
    final jsonString = await rootBundle.loadString(getAssetPath());
    final Map<String, dynamic> jsonMap = json.decode(jsonString);
    _localizedStrings = jsonMap.map(
      (key, value) => MapEntry(
        key,
        value.toString(),
      ),
    );
  }

  String getAssetPath() {
    final localeToUse = supportedLocales.firstWhere(
        (element) => element.languageCode == locale.languageCode,
        orElse: () => supportedLocales.first);

    return 'assets/i18n/${localeToUse.languageCode}.json';
  }

  String translate(String key) => _localizedStrings[key];
}

class _I18nDelegate extends LocalizationsDelegate<I18n> {
  const _I18nDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<I18n> load(Locale locale) async {
    final localizations = I18n(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(_I18nDelegate old) => false;
}
