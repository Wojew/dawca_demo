import 'dart:io';

import '../ui.dart';

class SdkHelper {
  static const _platformAndroid = MethodChannel('XXX');
  static const _platformIOS = MethodChannel('XXX');

  static const _updateWidgets = "updateWidgets";

  static Future<void> updateWidgets() async {
    if (Platform.isAndroid) {
      try {
        await _platformAndroid.invokeMethod(_updateWidgets);
      } on Exception catch (_) {}
    } else if (Platform.isIOS) {
      try {
        await _platformIOS.invokeMethod(_updateWidgets);
      } on Exception catch (_) {}
    }
  }
}
